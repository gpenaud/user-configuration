#! /usr/bin/ruby

# import librairies
require 'net/imap'
require 'json'
require 'timeout'
require 'libnotify'

# imap configuration
gmail       = {
    "server"         => "imap.gmail.org",
    "login"          => "guillaume.penaud@gmail.com",
    "password"       => "aej0Sude"
} 

openmailbox = {
    "server"         => "imap.openmailbox.org",
    "login"          => "guillaume.penaud",
    "password"       => "aej0Sude"
}


IMAP_CONNECTION_PARAMETERS = [gmail, openmailbox]
# cache configuration
CACHE_FILE          = ".cache_imap"

# scan maibox and return the number of unseen mails
def scan_mailboxes()
    nb_mail = 0

    begin
        # iterate on servers defined
        IMAP_CONNECTION_PARAMETERS.each { |server, connection|
            # set a max timeout
            Timeout::timeout(5) {
                # launch connection
                imap = Net::IMAP.new(connection["server"], 993, true)
                imap.login(connection["login"], connection["password"])
                imap.select('INBOX')

                nb_mail += imap.search(["NOT", "SEEN"]).count

                imap.logout
                imap.disconnect
            }    
        }
    rescue Net::IMAP::NoResponseError, Timeout::Error, SocketError
        # nothing to do more...
    end

    return nb_mail
end

def get_nb_mail()
    sentence = ""

    begin
        # reads the cache (need to be read before opening the file: size = 0)
        # need to read from IO - if im readin from file descriptor, JSON error... don't know why
        cache_data = JSON.parse(IO.read(CACHE_FILE))
        cache = File.open(CACHE_FILE, "w")
        nb_mail = scan_mailboxes()

        # if cache expiration date is still alive
        if nb_mail > cache_data["nb_mail"]
            sentence = nb_mail > 1 ? "%d new mails" : "%d new mail"
        end

        # reset the nb of mail in the cache
        cache_data["nb_mail"] = nb_mail

    rescue Exception => e
        cache = File.open(CACHE_FILE, "w")
        cache_data = {:nb_mail => scan_mailboxes()}
    end

    # write into the file
    JSON.dump(cache_data, cache)
    cache.close

    if !sentence.strip.empty? 
        Libnotify.show(
            :body => sprintf(sentence, nb_mail), 
            :summary => "checkmail", 
            :timeout => 2.5
        )    
    end
end

get_nb_mail()