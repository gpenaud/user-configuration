#! /bin/bash

POSTGRES_USER_ID=`id -u postgres`
DATA="/usr/local/pgsql/data"
LOG="/usr/local/pgsql/log/start_bdd.log"
NOW=`date`


if [ $UID -ne "$POSTGRES_USER_ID" ]; then
    printf "$NOW: %s\n" "l'utilisateur doit être postgres" >> $LOG
    exit 1
elif [ ! -d $DATA ]; then
    printf "$NOW: %s\n" "Le dossier data n'existe pas" >> $LOG
    exit 1
else
    /usr/local/pgsql/bin/pg_ctl -D $DATA start

    if [ $? -ne 0 ]; then
        printf "$NOW: %s\n" "Le démarrage du serveur ne s'est pas effectué correctement" >> $LOG
        exit 1
    fi
fi

