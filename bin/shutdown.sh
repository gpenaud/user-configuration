#!/bin/bash
while getopts "sr" opt; do
    case $opt in
      	s)          
	        sudo shutdown -h now
	        ;;
      	r)          
			sudo reboot
        	;;
      	*)
        	echo "unknown option"
        	;;
    esac
done
exit 0