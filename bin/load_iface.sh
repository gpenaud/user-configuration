#!/bin/bash
while getopts "i:" opt
  do
    case $opt in
      i)          
         case $OPTARG in
           wlan0)
             notify-send -u low -t 20 "network" "chargement de wlan0 en cours.."
             sudo ifdown wlan0 > /dev/null 2>&1 && sudo ifdown eth0 > /dev/null 2>&1 && sudo ifup wlan0 > /dev/null 2>&1
             ;;
           eth0)
             notify-send -u low -t 20 "network" "chargement de eth0 en cours.."
             sudo ifdown wlan0 > /dev/null 2>&1 && sudo ifdown eth0 > /dev/null 2>&1 && sudo ifup eth0 > /dev/null 2>&1
             ;;
           *)
             echo "unknown network interface"
             ;;
           esac
         ;;
      *)
         echo "unknown option"
         ;;
    esac
done
exit 0
