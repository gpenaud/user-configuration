#! /bin/bash

# check parameters
if [ $# -lt 1 ]
then 
    echo "Oops, missing parameter" 
    exit 1 
fi

function setCompletedPaths () {
    
    # try to replace ../ by absolute paths
    complete_path=$(readlink -e $parameter)
    
    # if relative path replaced by the absolute one, set it as parameter
    if [ ! $complete_path = ' ' ] 
    then 
	parameter=$complete_path 
    fi
    
    # retrieve the file name from a full path
    basename_param=$(basename $parameter)
    
    # if file name equals to script argument, need to complete path with current one
    if [ $basename_param = $parameter ]
    then 
	parameter=$(pwd)/$parameter
    fi
}

# validate param as valid files
function valid_param () {
    if [ ! -f $parameter ] && [ ! -d $parameter ]
    then
	echo "parameter $parameter must be a file or a directory"
	exit 2
    fi
}

# construct the index of file paths for restoration
function write_restoration_index () {
    index_path=$HOME/.Trash/.index
    if [ ! -f $index_path ]; then touch $index_path && chmod 600 $index_path; fi
    echo "$basename_param:$(dirname $parameter)" >> $index_path  
}

# trash creation if required
if [ ! -d $HOME/.Trash ]; then mkdir $HOME/.Trash; fi

for parameter in $*
do
   setCompletedPaths
   valid_param
   mv $parameter $HOME/.Trash
   write_restoration_index
done
