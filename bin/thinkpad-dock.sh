#!/bin/sh 
# Donne l'instance de X à traiter
export DISPLAY=:0

# 2 invocations because the Intel graphics card can only handle two outputs at a time
xrandr -d :0.0 --output LVDS1 --mode 1600x900
xrandr -d :0.0 --output VGA1 --auto --primary --mode 1920x1080 --left-of LVDS1

# Remise en place des workspaces
python /home/me/config/bin/i3plug.py restore

# On switch d'interface réseaux
/sbin/ifdown wlan0;
/sbin/ifup eth0;

# reconfigure X
xset -display :0.0 -dpms;
xset b off;
xset s off;

# on reload le wallpaper, au cas ou
feh --bg-scale /home/me/image/wallpaper/current.jpg

# NOTE pour le beeper
# rmmod pcspkr
# vim /etc/modprobe.d/blacklist
# blacklist pcspkr
