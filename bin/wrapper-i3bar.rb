#! /usr/bin/ruby

# import librairies
require 'json'

# custom standard output
def print_line(message)
    STDOUT.write message + "\n"
    STDOUT.flush
end

# method to stop reading when it's required
def read_line()
    # try reading a line, removing any extra whitespace
    begin
        line = STDIN.readline().strip
        # i3status sends EOF, or an empty line
        if line.empty?
        	exit 
        else 
        	return line
        end
    # exit on ctrl-c
    rescue SystemExit, Interrupt
	  raise
	rescue Exception => e
	  exit
	end
end

def get_dpms_state()
    dpms         = `xset -q`.include?("DPMS is Disabled") ? "no" : "yes"
    return sprintf("D:%s", dpms)
end

def get_screen_state()
    screensaver  = `xset -q`.include?("timeout:  0") ? "no" : "yes"
    return sprintf("S:%s", screensaver)
end

# Skip the first line which contains the version header.
print_line(read_line())

# The second line contains the start of the infinite array.
print_line(read_line())

while true
	line 	= read_line()
	prefix 	= ''

    # ignore comma at start of lines
    if line.start_with?(',')
        line, prefix = line[1..-1], ','
    end

    object = JSON.load(line)

    data = {
        1 => {"name" => "dpms",    "full_text"  => get_dpms_state()},
        2 => {"name" => "screen",  "full_text"  => get_screen_state()}
    }

    data.each do |index, info|
        object.insert(index, info)
    end

    puts prefix, JSON.generate(object)
    STDOUT.flush
end
