#! /bin/bash

rm -rf ~/.access-data .bash* bin .i3* .gitignore.d .vim* .xbindkeysrc .Xresources;
rm -rf ~/.config/vcsh/repo.d/user-configuration.git; 
rm -f  ~/.user-configuration-remover.sh
