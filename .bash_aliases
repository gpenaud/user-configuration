###########################
### alias configuration ###
###########################

alias bsrc='source ~/.zshrc'
alias gitalias='cat ~/.zshrc.d/aliases | grep git'

######################
### alias généraux ###
######################

alias ls='ls --color'
alias l='ls -lish'
alias ll='ls -lisah'
alias me='cd /home/me'
alias du='du -hs'
alias rm='rm -vI'

###########################
######## alias git ########
###########################

## commande de consultation ##
alias gs='git status'
alias gl='git log'
alias gd='git diff'
alias gdc='git diff --cached'
alias gsh='git show'
alias gbl='git blame'
alias gb='git branch'

## commande de manipulation du dépôt local ##
alias ga='git add'
alias gcm='git commit -m'
alias gca='git commit -a -m'
alias gcam='git commit --amend'
alias gck='git checkout'
alias grs='git reset --hard'
alias gcl='git reset HEAD'

## commande de fusion et de rebasement ##
alias gmg='git merge'
alias grb='git rebase'
alias gst='git stash'

## commande de fusion et de rebasement ##
alias gmg='git merge'
alias grb='git rebase'

## commande de manipulation de fichiers ##
alias gr='git rm'
alias gm='git mv'

## commande de manipulation de dépôts distants ##
alias gpl='git pull --rebase'
alias gpsh='gpl && git push'

###########################
######## alias soft #######
###########################
alias chut='sudo shutdown -h now'
alias ratiomaster='wine /home/me/soft/bin/ratiomaster/ratiomaster.exe'
alias groovedl='python /home/me/soft/bin/groove-dl/groove.py'

###########################
######## alias soft #######
###########################
alias lance='bash -x lance.sh'
alias argouml='java -jar /home/me/soft/argouml-0.34/argouml.jar'
alias pdf='mupdf'

###########################
###### alias divers #######
###########################
alias c='. mycd'
alias G='G'
alias h='$HOME/bin/help.sh'
alias mountel='sudo mount /dev/disk/by-id/usb-Samsung_File-CD_Gadget_0123456789ABCDEF-0:0 && sudo mount /dev/disk/by-id/usb-Samsung_File-CD_Gadget_0123456789ABCDEF-0:1'
alias umountel='sudo umount /media/main@phone && sudo umount /media/storage@phone'
alias chaf='c /home/me/taf/asrall'
alias ssha='ssh-add /home/me/.ssh/id_rsa'
alias aprl='apachectl -t && apachectl graceful'
alias pg='ping -c 2 google.fr'

###########################
###### alias screen #######
###########################
alias scls='screen -ls'
alias sccr='screen -S'
alias scr='screen -r'

###########################
###### alias virsh ########
###########################
alias virsh='virsh --connect qemu:///system'



